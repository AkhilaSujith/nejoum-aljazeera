<?php

class Common_model extends CI_Model{

    //get next entry no
    function get_next_entry_no(){
        $this->db->select('entry_no');
        $this->db->order_by('id','DESC');
	    $query=$this->db->get('journal_entry',1);
        if($query->num_rows()>0){
           $last_entry_no = $query->row('entry_no');
           $new_entry_no = $last_entry_no + 1;
        }
        else{
            $new_entry_no = 1001;
        }

        return $new_entry_no;
    }

    //get all rows from a table
    function get_all_rows($table, $search = array()){
        if(!empty($search)){
            foreach ($search as $field => $value) {
                if ($value != '')
                    $this->db->where($field, $value);
            }
        }
	    $query=$this->db->get($table);
        if($query->num_rows()>0){
          return $query->result_array();
        }
        else{
            return ;
        }
    }

    //function to insert data to table
    function insert($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
    }
    
    function update_with_array($table = '', $where = array(), $data = array())
	{
        return $this->db->update($table, $data, $where);  
    }

    function get_row($table, $search = array(), $select = '', $order_by = array())
	{
		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		if(!empty($order_by)){
			foreach ($order_by as $order_value => $order) {
				if ($order != '')
					$this->db->order_by($order_value, $order);
				else
					$this->db->order_by($order_value, 'ASC');
			}
        }
		$query = $this->db->get($table, 1);
		if ($query->num_rows() > 0) {
			if ($select != '')
				return $query->row($select);
			else
				return $query->row();
		} else {
			return;
		}
    }
    
    function array_column(array $input, $columnKey)
	{
		$array = array();
		foreach ($input as $value) {

			$array[] = $value[$columnKey];

		}
		return $array;
    }
    
    function delete($table = '', $data = array())
	{
		$this->db->delete($table, $data);
    } 
     
} ?>