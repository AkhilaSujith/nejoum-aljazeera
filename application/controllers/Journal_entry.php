<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journal_entry extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
		$this->load->library('upload');
		$this->load->model('Common_model');
	}
	
	public function index()
	{
		$arr = array(
			'id' => '',
			'entry_no' => $this->Common_model->get_next_entry_no(),
			'journal_file' => '',
			'today' => date('d/m/Y'),
			'accounts' => $this->Common_model->get_all_rows('accounts')
		);
		$this->load->view('journal_entry',$arr);
	}

	public function save_journal(){

		$journal_id = $this->input->post('journal_id');
		$update_date = date('Y-m-d H:i:s', time());
		$entry_no = $this->input->post('entry_no');
		$journal_date = $this->input->post('journal_date');
		$transfer_type = $this->input->post('transfer_type');
		$account_nos = $this->input->post('account');
		$debit = $this->input->post('debit');
		$credit = $this->input->post('credit');
		$description = $this->input->post('description');
		$creditaccount_nos = $this->input->post('creditaccount');
		$creditdebit = $this->input->post('creditdebit');
		$creditcredit = $this->input->post('creditcredit');
		$creditdescription = $this->input->post('creditdescription');
		$total_debit = $this->input->post('total_debit');
		$total_credit = $this->input->post('total_credit');
		$credit_id = $this->input->post('credit_id');
		$debit_id = $this->input->post('debit_id');

		$exploded_date = explode('/',$journal_date);
		$formatted_date = $exploded_date[2].'-'.$exploded_date[1].'-'.$exploded_date[0];

		if($journal_id == ''){
			$journal_table_array = array(
				'entry_no' => $entry_no,
				'entry_date' => $formatted_date,
				'transfer_type' => ($transfer_type != '') ? 1 : 0,
				'total_debit' => $total_debit,
				'total_credit' => $total_credit,
				'updated_date' => $update_date
			);

			$journal_entry_id = $this->Common_model->insert('journal_entry', $journal_table_array);
			$new_name = $journal_entry_id.'_'.rand();
			if($journal_entry_id > 0){
				$file_config = array(
					'upload_path' => "uploads/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "10240000" 
				);

				if ($_FILES['journal_file']['name'] != '') {
					$journal_table_file = $this->upload_file($file_config, 'uploads/', 'journal_file', FALSE);
					$this->Common_model->update_with_array('journal_entry',array('id'=>$journal_entry_id),array('file' => $journal_table_file));
				} 
				
				//enter debit details
				for($i = 0; $i < sizeof($account_nos); $i++){
					$data_to_save = array(
					  'journal_id' => $journal_entry_id,
					  'type' => 'Debit',
					  'account_no_id' => $account_nos[$i],
					  'debit' => $debit[$i],
					  'credit' => $credit[$i],
					  'description' => $description[$i],
					  'updated_date' => $update_date
					);
		  
					$debitinsert_id = $this->Common_model->insert('entries',$data_to_save);
				}

				//enter credit details
				for($i = 0; $i < sizeof($creditaccount_nos); $i++){
					$data_to_creditsave = array(
					  'journal_id' => $journal_entry_id,
					  'type' => 'Credit',
					  'account_no_id' => $creditaccount_nos[$i],
					  'debit' => $creditdebit[$i],
					  'credit' => $creditcredit[$i],
					  'description' => $creditdescription[$i],
					  'updated_date' => $update_date
					);
		  
					$creditinsert_id = $this->Common_model->insert('entries',$data_to_creditsave);
				}
				$msg= "Journal entry has been entered";
				$action = "alert-success";
				
				redirect('journal_entry/edit/'.$journal_entry_id.'/'.$msg.'/'.$action);
			}
		}
		else{
			$journal_detail = $this->Common_model->get_row('journal_entry',array('id' => $journal_id));

			//already existing credit and debit ids
			$debit_credit_details = $this->Common_model->get_all_rows('entries',array('journal_id' => $journal_id));
			$debit_credit_id_arr = $updated_ids_arr = array();

			if($debit_credit_details != ''){
			  $debit_credit_id_arr = $this->Common_model->array_column($debit_credit_details, 'id');
			}

			$new_name = $journal_id.'_'.rand();
			$file_config = array(
				'upload_path' => "uploads/",
				'allowed_types' => "*",
				'overwrite' => TRUE,
				'file_name' => $new_name,
				'max_size' => "10240000" 
			);

			if ($_FILES['journal_file']['name'] != '') {
				$journal_table_file = $this->upload_file($file_config, 'uploads/', 'journal_file', FALSE);
				if ($journal_detail->file != '' && file_exists('uploads/'.$journal_detail->file)) {
					unlink('uploads/' . $journal_detail->file);
				  }
			} 
			else{
				$journal_table_file = $journal_detail->file;
			}

			$journal_table_array = array(
				'entry_no' => $entry_no,
				'entry_date' => $formatted_date,
				'transfer_type' => ($transfer_type != '') ? 1 : 0,
				'total_debit' => $total_debit,
				'total_credit' => $total_credit,
				'file' => $journal_table_file,
				'updated_date' => $update_date
			);
			$this->Common_model->update_with_array('journal_entry',array('id'=>$journal_id), $journal_table_array);

			//enter debit details
			for($i = 0; $i < sizeof($account_nos); $i++){
				$data_to_save = array(
				  'journal_id' => $journal_id,
				  'type' => 'Debit',
				  'account_no_id' => $account_nos[$i],
				  'debit' => $debit[$i],
				  'credit' => $credit[$i],
				  'description' => $description[$i],
				  'updated_date' => $update_date
				);
				if($debit_id[$i] != ''){
					$this->Common_model->update_with_array('entries',array('id'=>$debit_id[$i]),$data_to_save);
					array_push($updated_ids_arr,$debit_id[$i]);
				}
				else{
					$debitinsert_id = $this->Common_model->insert('entries',$data_to_save);
				}
			}

			//enter credit details
			for($i = 0; $i < sizeof($creditaccount_nos); $i++){
				$data_to_creditsave = array(
				  'journal_id' => $journal_id,
				  'type' => 'Credit',
				  'account_no_id' => $creditaccount_nos[$i],
				  'debit' => $creditdebit[$i],
				  'credit' => $creditcredit[$i],
				  'description' => $creditdescription[$i],
				  'updated_date' => $update_date
				);
				if($credit_id[$i] != ''){
					$this->Common_model->update_with_array('entries',array('id'=>$credit_id[$i]),$data_to_creditsave);
					array_push($updated_ids_arr,$credit_id[$i]);
				}
				else{
					$creditinsert_id = $this->Common_model->insert('entries',$data_to_creditsave);
				}
			}

			//to delete removed entries from table
			$to_delete = array_diff($debit_credit_id_arr,$updated_ids_arr);
			if(!empty($to_delete)){
			  foreach($to_delete as $each){
				$this->Common_model->delete('entries',array('id'=>$each));
			  }
			}

			$msg= "Journal entry has been updated";
			$action = "alert-success";
			
			redirect('journal_entry/edit/'.$journal_id.'/'.$msg.'/'.$action);
		}
	}

	public function edit($journalid, $msg= '', $action = '')
	{
		$details = $this->Common_model->get_row('journal_entry',array('id' => $journalid));
		$debit_details = $this->Common_model->get_all_rows('entries',array('journal_id' => $journalid, 'type' => 'Debit'));
		$credit_details = $this->Common_model->get_all_rows('entries',array('journal_id' => $journalid, 'type' => 'Credit'));
		$arr = array(
			'id' =>  $journalid,
			'entry_no' => $details->entry_no,
			'journal_file' => $details->file,
			'today' => date('d/m/Y', strtotime($details->entry_date)),
			'accounts' => $this->Common_model->get_all_rows('accounts'),
			'transfer_type' => $details->transfer_type,
			'debit_details' => $debit_details,
			'credit_details' => $credit_details,
			'msg' => $msg,
			'action' => $action
		);
		$this->load->view('journal_entry',$arr);
	}

	function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
    {
      $this->upload->initialize($config);
      if ($this->upload->do_upload($file_name)) {
        $upload_data = $this->upload->data();
        $file_path = explode($destination, $upload_data['full_path']);
        $imagename = $filename = $file_path[1];
        if ($resize == FALSE){
          return $filename;
        }
        elseif ($resize == TRUE) {
          // settings for resize image
  
          $config['source_image'] = $upload_data['full_path'];
          $this->image_lib->clear();
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          if (file_exists($upload_data['full_path'])) {
            unlink($upload_data['full_path']);//after resize delete orginal image
          }
          $filename = explode('.', $filename);
          $image_thumb = $filename[0] . '_thumb.' . $filename[1];
          rename($destination . $image_thumb, $destination . $imagename);
          return $imagename;
        }
      }
      else{
        print_r($this->upload->display_errors());exit;
      }
	}
	
	public function journal_list()
	{
		$arr = array(
			'journals' => $this->Common_model->get_all_rows('journal_entry')
		);
		$this->load->view('journal_list',$arr);
	}
}
