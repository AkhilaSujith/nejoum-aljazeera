<?php $this->load->view('header');?>

		<div class="row">
			<h3>Journal List</h3>
		</div>

		<div class="add-journal-entry">
			<div class="row">
					<p class="journal-list-p">
						<a href="<?php echo base_url();?>journal_entry" class="btn btn-primary">Add Journal</a>
					</p>
			</div>
		<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Entry No</th>
					<th>File Name</th>
					<th>Entry Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php if($journals){
					$i = 1;
					foreach($journals as $each){ ?>
						<tr>
							<td><?php echo $i++;?></td>
							<td><?php echo $each['entry_no'];?></td>
							<td><?php echo $each['file'];?></td>
							<td><?php echo date('d/m/Y', strtotime($each['entry_date']));?></td>
							<td><a href="<?php echo base_url();?>journal_entry/edit/<?php echo $each['id'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
						</tr>
				<?php
					}
				} ?>
			</tbody>
		</table>			
		</div>
	</div>
<?php $this->load->view('footer');?>
  </body>
</html>
<script>
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>