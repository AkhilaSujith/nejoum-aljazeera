<?php $this->load->view('header');?>

		<div class="row">
			<h3>Journal Entry</h3>
		</div>

		<div class="add-journal-entry">
			<div class="row">
					<p class="journal-list-p">
						<a href="<?php echo base_url();?>journal_entry/journal_list" class="btn btn-primary">Journal List</a>
					</p>
			</div>
			<?php if(isset($msg)){
                        $message = $msg;
                    }
                    else
                        $message =''; 
                   
                    if($message!='')
                    { ?>
                        <div class="alert <?php echo $action;?> alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
            <?php 
					}  
			?>
			<?php echo form_open_multipart('journal_entry/save_journal'); ?>
				<input type="hidden" name="journal_id" id="journal_id" value="<?php echo $id; ?>" />
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Entry No:</label>
							<input type="hidden" name="entry_no" value="<?php echo $entry_no;?>">
							<span><?php echo $entry_no;?></span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="image-upload">
								<label for="file-input">Attach File
								<i class="fa fa-file"></i>
								</label>
								<input id="file-input" type="file" name="journal_file"/>
								<?php echo $journal_file;?>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group disp-inl-flex">
							<label>Date</label>
							<div class="input-group date">
								<input type="text" class="form-control date" id="journal_date" name="journal_date" value="<?php echo $today; ?>" placeholder="dd/mm/yyyy">
								<div class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</div>

							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Transfer Type</label>
							<input type="checkbox" name="transfer_type" class="input-control" <?php if(isset($transfer_type) && $transfer_type == 1) echo 'checked';?>>
 						</div>
					</div>
				</div>

				<div class="debit-section">
					<?php if(isset($debit_details)){
						foreach($debit_details as $each){ ?>
							<div class="row">
								<div class="col-md-1 txtend">
									<input type="hidden" name="debit_id[]" value="<?php echo $each['id'];?>">
									<a href="javascript:void(0);" class="addButton addremoveicon" title="Add field">
										<i class="fa fa-plus-circle green-color"></i>
									</a>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<select class="form-control" name="account[]" >
											<?php foreach($accounts as $each_count) { ?>
												<option value="<?php echo $each_count['id'];?>" <?php if($each['account_no_id'] == $each_count['id']) echo 'selected';?>><?php echo $each_count['account_no'];?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<input type="number" class="form-control eachdebit" name="debit[]" value="<?php echo $each['debit'];?>" onblur="tally();">
									</div>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<input type="number" class="form-control eachcredit" name="credit[]" value="<?php echo $each['credit'];?>" onblur="tally();">
									</div>
								</div>

								<div class="col-md-4 pdlr0">
									<div class="form-group">
										<textarea class="form-control journal-desc" name="description[]"><?php echo $each['description'];?></textarea>
									</div>
								</div>

								<div class="col-md-1 txtleft">
									<a href="javascript:void(0);" class="removeButton addremoveicon" title="Add field">
										<i class="fa fa-minus-circle red-color"></i>
									</a>
								</div>
							</div>
					<?php }
					} 
					else { ?>
						<div class="row">
							<div class="col-md-1 txtend">
								<input type="hidden" name="debit_id[]" value="">
								<a href="javascript:void(0);" class="addButton addremoveicon" title="Add field">
									<i class="fa fa-plus-circle green-color"></i>
								</a>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<select class="form-control" name="account[]" >
										<?php foreach($accounts as $each_count) { ?>
											<option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<input type="number" class="form-control eachdebit" name="debit[]" value="0" onblur="tally();">
								</div>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<input type="number" class="form-control eachcredit" name="credit[]" value="0" onblur="tally();">
								</div>
							</div>

							<div class="col-md-4 pdlr0">
								<div class="form-group">
									<textarea class="form-control journal-desc" name="description[]"></textarea>
								</div>
							</div>

							<div class="col-md-1 txtleft">
								<a href="javascript:void(0);" class="removeButton addremoveicon" title="Add field">
									<i class="fa fa-minus-circle red-color"></i>
								</a>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="field_wrapperdebit" class="field_wrapperdebit"></div>

				<div class="credit-section">
					<?php if(isset($credit_details)){
						foreach($credit_details as $each){ ?>
							<div class="row">
								<div class="col-md-1 txtend">
									<input type="hidden" name="credit_id[]" value="<?php echo $each['id'];?>">
									<a href="javascript:void(0);" class="addButtonCredit addremoveicon" title="Add field">
										<i class="fa fa-plus-circle lightgreen-color"></i>
									</a>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<select class="form-control" name="creditaccount[]" >
											<?php foreach($accounts as $each_count) { ?>
												<option value="<?php echo $each_count['id'];?>" <?php if($each_count['id'] == $each['account_no_id']) echo 'selected';?>><?php echo $each_count['account_no'];?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<input type="number" class="form-control eachdebit" name="creditdebit[]" value="<?php echo $each['debit'];?>" onblur="tally();">
									</div>
								</div>

								<div class="col-md-2 pdlr0">
									<div class="form-group">
										<input type="number" class="form-control eachcredit" name="creditcredit[]" value="<?php echo $each['credit'];?>"  onblur="tally();">
									</div>
								</div>

								<div class="col-md-4 pdlr0">
									<div class="form-group">
										<textarea class="form-control journal-desc" name="creditdescription[]"><?php echo $each['description'];?></textarea>
									</div>
								</div>

							<div class="col-md-1 txtleft">
								<a href="javascript:void(0);" class="removeButtonCredit addremoveicon" title="Add field">
									<i class="fa fa-minus-circle red-color"></i>
								</a>
							</div>
						</div>
					<?php 
						}
					}
					else { ?>
						<div class="row">
							<div class="col-md-1 txtend">
								<input type="hidden" name="credit_id[]" value="">
								<a href="javascript:void(0);" class="addButtonCredit addremoveicon" title="Add field">
									<i class="fa fa-plus-circle lightgreen-color"></i>
								</a>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<select class="form-control" name="creditaccount[]" >
										<?php foreach($accounts as $each_count) { ?>
											<option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<input type="number" class="form-control eachdebit" name="creditdebit[]" value="0" onblur="tally();">
								</div>
							</div>

							<div class="col-md-2 pdlr0">
								<div class="form-group">
									<input type="number" class="form-control eachcredit" name="creditcredit[]" value="0" onblur="tally();">
								</div>
							</div>

							<div class="col-md-4 pdlr0">
								<div class="form-group">
									<textarea class="form-control journal-desc" name="creditdescription[]"></textarea>
								</div>
							</div>

							<div class="col-md-1 txtleft">
								<a href="javascript:void(0);" class="removeButtonCredit addremoveicon" title="Add field">
									<i class="fa fa-minus-circle red-color"></i>
								</a>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="field_wrappercredit" class="field_wrappercredit"></div>

				<div class="row">
					<div class="col-md-3">&nbsp;</div>
					<div class="col-md-2">
						<input type="submit" id="savebtn" name="Save" value="Save" class="btn btn-save">
					</div>
					<div class="col-md-2 btn-debit" id="totaldebitbtn">Total Debit</div>
					<div class="col-md-2 btn-credit" id="totalcreditbtn">Total Credit</div>
					<input type="hidden" id="total_debit" name="total_debit" value="0">
					<input type="hidden" id="total_credit" name="total_credit" value="0">
				</div>
		</div>
	</div>

	<?php $this->load->view('footer');?>
  </body>
</html>
<script>
$(document).ready(function() {
	tally();
  moment.updateLocale('en', {
    week: {dow: 1} // Monday is the first day of the week
  });

  $('.date').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'en'
  });

	var wrapper = $('.field_wrapperdebit'); //Input field wrapper
	var addButton = $('.addButton');
	var x = 1; //Initial field counter is 1
	$(addButton).click(function (){ //Once add button is clicked
		x++; //Increment field counter
		var fieldHTML = '<div class="row debit-section"><div class="col-md-1 txtend"><input type="hidden" name="debit_id[]" value=""><a href="javascript:void(0);" class="addButton addremoveicon" title="Add field"><i class="fa fa-plus-circle green-color"></i></a></div><div class="col-md-2 pdlr0"><div class="form-group"><select class="form-control" name="account[]" ><?php foreach($accounts as $each_count) { ?><option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option><?php } ?></select></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachdebit" name="debit[]" value="0" onblur="tally();"></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachcredit" name="credit[]" value="0" onblur="tally();"></div></div><div class="col-md-4 pdlr0"><div class="form-group"><textarea class="form-control journal-desc" name="description[]"></textarea></div></div><div class="col-md-1 txtleft"><a href="javascript:void(0);" class="removeButton addremoveicon" title="Add field"><i class="fa fa-minus-circle red-color"></i></a></div></div>'; //New input field html 
		
		$( wrapper).append(fieldHTML); // Add field html
	});
	$('.removeButton').click(function (e) { //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').parent('div').remove(); //Remove field html
		x--; //Decrement field counter
		tally();
	});
	$(wrapper).on('click', '.addButton', function (){ //Once add button is clicked
		x++; //Increment field counter
		var fieldHTML = '<div class="row debit-section"><div class="col-md-1 txtend"><input type="hidden" name="debit_id[]" value=""><a href="javascript:void(0);" class="addButton addremoveicon" title="Add field"><i class="fa fa-plus-circle green-color"></i></a></div><div class="col-md-2 pdlr0"><div class="form-group"><select class="form-control" name="account[]" ><?php foreach($accounts as $each_count) { ?><option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option><?php } ?></select></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachdebit" name="debit[]" value="0" onblur="tally();"></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachcredit" name="credit[]" value="0" onblur="tally();"></div></div><div class="col-md-4 pdlr0"><div class="form-group"><textarea class="form-control journal-desc" name="description[]"></textarea></div></div><div class="col-md-1 txtleft"><a href="javascript:void(0);" class="removeButton addremoveicon" title="Add field"><i class="fa fa-minus-circle red-color"></i></a></div></div>'; //New input field html 
		
		$( wrapper).append(fieldHTML); // Add field html
	});
	$(wrapper).on('click', '.removeButton', function (e) { //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').parent('div').remove(); //Remove field html
		x--; //Decrement field counter
		tally();
	});

	var wrapper2 = $('.field_wrappercredit'); //Input field wrapper
	var addButtoncredit = $('.addButtonCredit');
	var y = 1; //Initial field counter is 1
	$(addButtoncredit).click(function () { //Once add button is clicked
		y++; //Increment field counter
		var fieldHTML2 = '<div class="row credit-section"><div class="col-md-1 txtend"><input type="hidden" name="credit_id[]" value=""><a href="javascript:void(0);" class="addButtonCredit addremoveicon" title="Add field"><i class="fa fa-plus-circle lightgreen-color"></i></a></div><div class="col-md-2 pdlr0"><div class="form-group"><select class="form-control" name="creditaccount[]" ><?php foreach($accounts as $each_count) { ?><option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option><?php } ?></select></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachdebit" name="creditdebit[]" value="0" onblur="tally();"></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachcredit" name="creditcredit[]" value="0" onblur="tally();"></div></div><div class="col-md-4 pdlr0"><div class="form-group"><textarea class="form-control journal-desc" name="creditdescription[]"></textarea></div></div><div class="col-md-1 txtleft"><a href="javascript:void(0);" class="removeButtonCredit addremoveicon" title="Add field"><i class="fa fa-minus-circle red-color"></i></a></div></div>'; //New input field html 
		
		$( wrapper2).append(fieldHTML2); // Add field html
	});
	$('.removeButtonCredit').click(function (e) { //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').parent('div').remove(); //Remove field html
		y--; //Decrement field counter
		tally();
	});
	$(wrapper2).on('click', '.addButtonCredit', function () { //Once add button is clicked
		y++; //Increment field counter
		var fieldHTML2 = '<div class="row credit-section"><div class="col-md-1 txtend"><input type="hidden" name="credit_id[]" value=""><a href="javascript:void(0);" class="addButtonCredit addremoveicon" title="Add field"><i class="fa fa-plus-circle lightgreen-color"></i></a></div><div class="col-md-2 pdlr0"><div class="form-group"><select class="form-control" name="creditaccount[]" ><?php foreach($accounts as $each_count) { ?><option value="<?php echo $each_count['id'];?>"><?php echo $each_count['account_no'];?></option><?php } ?></select></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachdebit" name="creditdebit[]" value="0" onblur="tally();"></div></div><div class="col-md-2 pdlr0"><div class="form-group"><input type="number" class="form-control eachcredit" name="creditcredit[]" value="0" onblur="tally();"></div></div><div class="col-md-4 pdlr0"><div class="form-group"><textarea class="form-control journal-desc" name="creditdescription[]"></textarea></div></div><div class="col-md-1 txtleft"><a href="javascript:void(0);" class="removeButtonCredit addremoveicon" title="Add field"><i class="fa fa-minus-circle red-color"></i></a></div></div>'; //New input field html 
		
		$( wrapper2).append(fieldHTML2); // Add field html
	});
	$(wrapper2).on('click', '.removeButtonCredit', function (e) { //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').parent('div').remove(); //Remove field html
		y--; //Decrement field counter
		tally();
	});
});

function tally(){
	var totaldebit = 0;
	var totalcredit = 0;
	$('.eachdebit').each(function() { 
		totaldebit = +totaldebit + +$(this).val(); 
	});
	$('.eachcredit').each(function() { 
		totalcredit = +totalcredit + +$(this).val(); 
	});

	document.getElementById("totaldebitbtn").innerHTML = totaldebit;
	document.getElementById("totalcreditbtn").innerHTML = totalcredit;
	$('#total_debit').val(totaldebit);
	$('#total_credit').val(totalcredit);

	if(totaldebit != 0 && totalcredit != 0){
		if(totaldebit != totalcredit){
			document.getElementById('savebtn').disabled = true;
		}
		else{
			document.getElementById('savebtn').disabled = false;
		}
	}
	else{
		document.getElementById('savebtn').disabled = true;
	}
	
}
</script>

