<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>NEJOUM ALJAZEERA USED CARS LLC</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>style.css">

		<!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
        
		<!-- Datepicker-->
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap-datetimepicker.min.css" rel="stylesheet" />
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap-datepicker.min.css" rel="stylesheet">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
		<!-- Datepicker -->
		<script src="<?php echo HTTP_JS_PATH;?>moment.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH;?>bootstrap-datetimepicker.min.js"></script>
		 
	</head>
  <body>
	<div class="container text-center">
		<div class="row logo-row">
			<img src="<?php echo HTTP_UPLOADS_PATH;?>logo.png" class="logo-img">
		</div>
